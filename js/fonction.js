//--------------------------------------------------------------------------------------------------------------------

                                      //PARAMETRE GENERAUX 

//--------------------------------------------------------------------------------------------------------------------

var map = L.map('map', {
    center: [45.63, 5.9],      // Centre de la map
    zoom:9,
    zoomControl: true ,       // nécessaire pour changer la position du zoom sur la carte    
/*     maxBounds: [              // limite la navigation              
        [45, 5.4],
        [46, 7.2]
    ], */
});
map.zoomControl.setPosition('bottomright');

var hash = new L.Hash(map);
map.attributionControl.addAttribution('<a href="https://github.com/tomchadwin/qgis2web" target="_blank">qgis2web</a>');






//--------------------------------------------------------------------------------------------------------------------

                                      //FOND OSM 

//--------------------------------------------------------------------------------------------------------------------

var osm = L.tileLayer('http://a.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    opacity: 1.0
});
osm.addTo(map);






//--------------------------------------------------------------------------------------------------------------------

                                      // COUCHE COMMUNES  

//--------------------------------------------------------------------------------------------------------------------

// Style
function styleCom18() {
    return {
        pane: 'paneCom18',
        opacity: 1,
        color: 'rgba(50,67,79,1.0)',
        dashArray: '',
        lineCap: 'butt',
        lineJoin: 'miter',
        weight: 1.0, 
        fill: true,
        fillOpacity: 1,
        fillColor: 'rgba(157,118,200,0.0)',
        }
}

// Indexation commune
map.createPane('paneCom18');
map.getPane('paneCom18').style.zIndex = 401;
map.getPane('paneCom18').style['mix-blend-mode'] = 'normal';

// Affichage de la couche
var com18 = new L.geoJson(com18Geojson, {
    attribution: '<a href="https://agate-territoires.fr/">AGATE</a>',
    pane: 'pane_com18_73_1',
    style: styleCom18,
}).addTo(map);








//--------------------------------------------------------------------------------------------------------------------

                                      // COUCHE Territoires  

//--------------------------------------------------------------------------------------------------------------------

var terr1 = {color: "#3C4D59",
            weight:0.8,
            opacity:1,
            fillOpacity:0.8};

var terr2 = {color: "#3C4D59",
            weight:0.8,
            opacity:1,
            fillOpacity:0.8};

var terr3 = {color: "#3C4D59",
            weight:0.8,
            opacity:1,
            fillOpacity:0.8};

var terr4 = {color: "#3C4D59",
            weight:0.8,
            opacity:1,
            fillOpacity:0.8};

var terr5 = {color: "#3C4D59",
            weight:0.8,
            opacity:1,
            fillOpacity:0.8};

var terr6 = {color: "#3C4D59",
            weight:0.8,
            opacity:1,
            fillOpacity:0.8};

var terr7 = {color: "#3C4D59",
            weight:0.8,
            opacity:1,
            fillOpacity:0.8};
// Style
function styleTerr(features) {
    var features = features.properties.TERRITOIRE;
        switch (features) {
            case "Combe de Savoie" : 
            return terr1
            case "Tarentaise" : 
            return terr2
            case "Maurienne" : 
            return terr3
            case "Chambéry" : 
            return terr4
            case "Alberville - Ugine" : 
            return terr5
            case "Aix-les-Bains" : 
            return terr6
            case "Avant Pays Savoyard" : 
            return terr7
        } 
};

// Indexation commune
map.createPane('paneTerr');
map.getPane('paneTerr').style.zIndex = 400;
map.getPane('paneTerr').style['mix-blend-mode'] = 'normal';

// Affichage de la couche
var com18 = new L.geoJson(terrGeojson, {
    pane: 'paneTerr',
    style: styleTerr,
}).addTo(map);






//--------------------------------------------------------------------------------------------------------------------

                                      //AXES ICON

//--------------------------------------------------------------------------------------------------------------------

// Création d'une classe icon générale
var marker = L.Icon.extend({
    options: {
        iconSize: [40,60],
        iconAnchor: [20, 42]
    }
});

// Ajout des icons à la classe
var iconSante = new marker({iconUrl: "images/sante.png"});
var iconSocial = new marker({iconUrl: "images/social.png"});
var iconEnvironnement = new marker({iconUrl: "images/environnement.png"});
var iconRetrait = new marker({iconUrl: "images/retraite.png"});
var iconSpasad = new marker({iconUrl: "images/Spasad.png"});


// Fonction filtre pour sélectionner les axes
function axeSante(features){
    if (features.properties.axe_concer === "1"){
        console.log("hey");
        return features.properties.axe_concer
    }
}


function axeSocial(features){
    if (features.properties.axe_concer === "2"){
        console.log("hey");
        return features.properties.axe_concer
    }
}

function axeEnvironnement(features){
    if (features.properties.axe_concer === "3"){
        console.log("hey");
        return features.properties.axe_concer
    }
}

function axeRetrait(features){
    if (features.properties.axe_concer === "4"){
        console.log("hey");
        return features.properties.axe_concer
    }
}

function axeSpasad(features){
    if (features.properties.axe_concer === "5"){
        console.log("hey");
        return features.properties.axe_concer
    }
}








//--------------------------------------------------------------------------------------------------------------------

                                      //FONCTION POPUP AXE

//--------------------------------------------------------------------------------------------------------------------


function onEachFeature(feature,layer){   

    // POPUP
    layer.on('click', function(e){
        
         $('#popup').remove();
         // Nom de l'action
         var nomAction = layer.feature.properties.Nom_action;

         // Picto 
         if (layer.feature.properties.axe_concer === "1"){
             var image = '<img src = "images/sante.png" alt = "pictoSante" class = pictoLegende>'
         }else if(layer.feature.properties.axe_concer === "2"){
             var image = '<img src = "images/social.png" alt = "pictoSante" class = pictoLegende>'
         }else if(layer.feature.properties.axe_concer === "3"){
             var image = '<img src = "images/environnement.png" alt = "pictoSante" class = pictoLegende>' 
         }else if(layer.feature.properties.axe_concer === "4"){
             var image = '<img src = "images/retraite.png" alt = "pictoSante" class = pictoLegende>' 
         }else if(layer.feature.properties.axe_concer === "5"){
             var image = '<img src = "images/Spasad.png" alt = "pictoSante" class = pictoLegende>' 
         } 
    

         // Calendrier
/*          if (layer.feature.properties.Calendrier != null){
            var calendrier = layer.feature.properties.Calendrier;
         }else{
             var calendrier = "Donnée non disponible"
             
         }; */

        // Adresse
         if (layer.feature.properties.adresse_nu === null){
            var adresse = layer.feature.properties.adresse_ru + '</br>'+ layer.feature.properties.adresse_co + ' '+ layer.feature.properties.adresse__1

         }else{
            var adresse = layer.feature.properties.adresse_nu + ' ' + layer.feature.properties.adresse_ru + '</br>'+ layer.feature.properties.adresse_co + ' '+ layer.feature.properties.adresse__1
             
         };
       

        // Programme
         if (layer.feature.properties.Programme != null){
            var programme = layer.feature.properties.Programme;
         }else{
            var programme = "Donnée non disponible"
         };

        // Porteur projet
         if (layer.feature.properties.Nom_porte != null){
            var porteur = layer.feature.properties.Nom_porte;
         }else{
            var porteur = "Donnée non disponible"
         };
   
        // Téléphone
         if (layer.feature.properties.telephone != null){
            var tel = layer.feature.properties.telephone;
         }else{
            var tel = "Donnée non disponible"
         };
        
         // Site
         if (layer.feature.properties.Site_inter != null){
            var site = '<a href = "https://' + layer.feature.properties.Site_inter +'">'+ layer.feature.properties.Site_inter + '</a>';
         }else{
            var site = "Donnée non disponible"
         };

         
         // Création de la div popup
         $('<div id="popup"><div class = "titrePopup">' + image + '<h3>' +
         nomAction +'</h3><img src = "images/picto_fermeture.png" alt = "pictoFermet" id = pictoFermeture></div><div class = "infoPopup"><h4> Mots à mettre </br></h4><span></span></div><div class ="infoPopup"><h4>Adresse : </br></h4><span>' +
         adresse + '</span></div><div class = "infoPopup"><h4>Consulter le programme : </br></h4><span>' + 
         programme +'</span></div><div class = "infoPopup"><h4>Organisé par : </br></h4><span>' +
         porteur + '</span></div><div class = "infoPopup"><h4>Tel : </br></h4><span>' +
         tel + '</span></div><div class = "infoPopup"><h4>Site internet : </br></h4><span>' +
         site+ '</span></div>').insertBefore('#map');

         $('body').on('click', '#pictoFermeture', function () {
            console.log("etape1");
            $('#popup').remove();      // On enlève les popup quand l'utilisateur clic que le picto fermeture (cf. css)
        });  
        

    });

};
 






//--------------------------------------------------------------------------------------------------------------------

                                      // RADIO BUTTON
                                      
//--------------------------------------------------------------------------------------------------------------------

var selectRadio = L.layerGroup([]);          // Création d'un groupe vide
selectRadio.addTo(map);                      

// Si la couche est sélectionnée dans le menu
var santeSelect = document.getElementById("sante").addEventListener('click', function(event){
    selectRadio.clearLayers();               // Permet d'effacer le contenu du groupe   
    selectRadio.addLayer(axeSante);       // Ajout de la couche dans le groupe
    selectRadio.addTo(map);                  // On affiche le groupe
});

var socialSelect = document.getElementById("social").addEventListener('click', function(event){
    selectRadio.clearLayers();               
    selectRadio.addLayer(axeSocial);      
    selectRadio.addTo(map);                  
});

var environnementSelect = document.getElementById("environnement").addEventListener('click', function(event){
    selectRadio.clearLayers();               
    selectRadio.addLayer(axeEnvironnement);      
    selectRadio.addTo(map);                  
});

var retraiteSelect = document.getElementById("retraite").addEventListener('click', function(event){
    selectRadio.clearLayers();               
    selectRadio.addLayer(axeRetraite);      
    selectRadio.addTo(map);                  
});

var spasadSelect = document.getElementById("SPASAD").addEventListener('click', function(event){
    selectRadio.clearLayers();               
    selectRadio.addLayer(axeSpasad);       
    selectRadio.addTo(map);                 
});










//--------------------------------------------------------------------------------------------------------------------

                                      //AFFICHAGE DES AXES

//--------------------------------------------------------------------------------------------------------------------

// Affichage de la couche
var axeSante = new L.geoJson(axesData, {
    // Fonction filtre axe
    filter: axeSante,
    // Fonction popup
    onEachFeature: onEachFeature,
    // Fonction pour l'icon
    pointToLayer: function(feature,latlng){
        return L.marker(latlng, {icon: iconSante})
    }
});

var axeSocial = new L.geoJson(axesData, {
    filter: axeSocial,
    onEachFeature: onEachFeature,
    pointToLayer: function(feature,latlng){
        return L.marker(latlng, {icon: iconSocial})
    }
});

var axeEnvironnement = new L.geoJson(axesData, {
    filter: axeEnvironnement,
    onEachFeature: onEachFeature,
    pointToLayer: function(feature,latlng){
        return L.marker(latlng, {icon: iconEnvironnement})
    }
});

var axeRetraite = new L.geoJson(axesData, {
    filter: axeRetrait,
    onEachFeature: onEachFeature,
    pointToLayer: function(feature,latlng){
        return L.marker(latlng, {icon: iconRetrait})
    }
});

var axeSpasad = new L.geoJson(axesData, {
    filter: axeSpasad,
    onEachFeature: onEachFeature,
    pointToLayer: function(feature,latlng){
        return L.marker(latlng, {icon: iconSpasad})
    }

});






